package main.java.com.luxoft.calcSWT.service;

import java.math.BigDecimal;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.jupiter.api.Test;

import main.java.com.luxoft.calcSWT.model.CalculatorModel;
import main.java.com.luxoft.calcSWT.model.Operand;

class CalculatorServiceTest {

	BigDecimal i1;
	BigDecimal i2;
	
	@Before
	void init() {
		
		i1 = new BigDecimal(10);
		i2 = new BigDecimal(20);
		
	}
	
	@After
	void cleanUp() {
		
	}

	@Test
	void testPlus() {
		
		CalculatorModel answer = CalculatorService.operationPerform(i1, Operand.PLUS , i2);
		
		BigDecimal actual = answer.getResult();
		BigDecimal expected = new BigDecimal(30);
		
		Assert.assertEquals(expected, actual);
	}
	
	@Test
	void testPlusTrans() {
		
		CalculatorModel answer = CalculatorService.operationPerform(i2, Operand.PLUS , i1);
		
		BigDecimal actual = answer.getResult();
		BigDecimal expected = new BigDecimal(30);
		
		Assert.assertEquals(expected, actual);
	}
	
	@Test
	void testMinus() {
		
		CalculatorModel answer = CalculatorService.operationPerform(i1, Operand.MINUS , i2);
		
		BigDecimal actual = answer.getResult();
		BigDecimal expected = new BigDecimal(-10);
		
		Assert.assertEquals(expected, actual);
	}
	
	@Test
	void testMultiply() {
		
		CalculatorModel answer = CalculatorService.operationPerform(i1, Operand.MULTIPLY , i2);
		
		BigDecimal actual = answer.getResult();
		BigDecimal expected = new BigDecimal(200);
		
		Assert.assertEquals(expected, actual);
	}
	
	@Test
	void testMultiplyFloat() {
		
		i1 = new BigDecimal(21.2);
		i2 = new BigDecimal(206.45);
		
		CalculatorModel answer = CalculatorService.operationPerform(i1, Operand.MULTIPLY , i2);
		
		Double actual = Double.valueOf(answer.getResult().toString());
		Double expected = 4376.740;
		
		Assert.assertEquals(expected, actual);
	}
	
	@Test
	void testDivide() {
		
		i1 = new BigDecimal(21.2);
		i2 = new BigDecimal(206.45);
		
		CalculatorModel answer = CalculatorService.operationPerform(i1, Operand.DIVIDE , i2);
		
		Double actual = Double.valueOf(answer.getResult().toString());
		Double expected = 0.102688302252361;
		
		Assert.assertEquals(expected, actual);
	}
	
	@Test
	void testDivideTrans() {
		
		CalculatorModel answer = CalculatorService.operationPerform(i1, Operand.DIVIDE , i1);
		CalculatorModel answer2 = CalculatorService.operationPerform(i2, Operand.DIVIDE , i2);
		
		BigDecimal actual = answer.getResult();
		BigDecimal expected = answer2.getResult();
		
		Assert.assertEquals(expected, actual);
	}
}
