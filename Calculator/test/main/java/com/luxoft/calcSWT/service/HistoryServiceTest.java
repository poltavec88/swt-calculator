package main.java.com.luxoft.calcSWT.service;

import java.math.BigDecimal;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import main.java.com.luxoft.calcSWT.model.CalculatorModel;
import main.java.com.luxoft.calcSWT.model.Operand;

class HistoryServiceTest {
	CalculatorModel calculatorModel;
	
	@Before
	void setUp() throws Exception {
		calculatorModel = CalculatorService.operationPerform(new BigDecimal(12), Operand.PLUS, new BigDecimal(10));
		
	}

	@Test
	void addHistoryItemTest() {
		HistoryService.getHistoryService().addHistoryItem(calculatorModel);
		HistoryService.getHistoryService().addHistoryItem(calculatorModel);
		HistoryService.getHistoryService().addHistoryItem(calculatorModel);
		
		int expected = 3;
		int actual = HistoryService.getHistoryService().getHisSize();
		
		Assert.assertEquals(expected, actual);
	}

}
