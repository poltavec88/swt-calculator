package main.java.com.luxoft.calcSWT.model;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class HistoryModel {
	
	private List<BigDecimal> input1 = new ArrayList<>();
	private List<Operand> operand = new ArrayList<>();
	private List<BigDecimal> input2 = new ArrayList<>();
	private List<BigDecimal> result = new ArrayList<>();
		
	public  List<BigDecimal> getInput1() {
		return input1;
	}
		
	public List<Operand> getOperand() {
		return operand;
	}

	public List<BigDecimal> getInput2() {
		return input2;
	}

	public List<BigDecimal> getResult() {
		
		return result;
	}
	
	private boolean vefityIndex(int i) {
		if (i < input1.size() && i > -1)
			if (i < input2.size() && i > -1)
				if (i < operand.size() && i > -1)
					if (i < result.size() && i > -1)
						return true;
		return false;
	}
	
	public String toStringHis(int i) {
		if (vefityIndex(i)) {
			return (i+1) + ": " + input1.get(i) + " " + operand.get(i).getValue() + " " + input2.get(i) + " = " 
						+ Double.valueOf(result.get(i).toString());
		} 
		else 
			return "";
	}
}
