package main.java.com.luxoft.calcSWT.model;

import java.math.BigDecimal;

public class CalculatorModel {
	
	private BigDecimal firstNumber;
	private BigDecimal secondNumber;
	private Operand operand;
	private BigDecimal result;
	
	public BigDecimal getFirstNumber() {
		return firstNumber;
	}
	
	public void setFirstNumber(BigDecimal firstNumber) {
		this.firstNumber = firstNumber;
	}
	
	public BigDecimal getSecondNumber() {
		return secondNumber;
	}
	
	public void setSecondNumber(BigDecimal secondNumber) {
		this.secondNumber = secondNumber;
	}
	
	public Operand getOperand() {
		return operand;
	}
	
	public void setOperand(Operand operand) {
		this.operand = operand;
	}
	
	public BigDecimal getResult() {
		return result;
	}
	
	public void setResult(BigDecimal result) {
		this.result = result;
	}
	
	@Override
	public String toString() {
		return firstNumber + " " + operand.getValue() + " " + secondNumber + " = " + Double.valueOf(result.toString());
	}
	
		
}
