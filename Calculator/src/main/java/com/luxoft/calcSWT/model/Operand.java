package main.java.com.luxoft.calcSWT.model;

public enum Operand {
	
	PLUS("+"), MINUS("-"), MULTIPLY("*"), DIVIDE("/"), UNKNOWN("");
	 
    private String value;
 
    Operand(String value) {
        this.value = value;
    }

	public String getValue() {
		return value;
	}
	
	public static Operand getFromString(String str) {
		switch (str) {
        case "+": 
     	   
     	   return Operand.PLUS;
     	   
        case "-": 
     	   
        	return Operand.MINUS;
     	   
        case "*": 
     	   
        	return Operand.MULTIPLY;
     	   
        case "/": 
     	   
        	return Operand.DIVIDE;
     	   
        default: 
     	   return Operand.UNKNOWN;
		}
	}
}