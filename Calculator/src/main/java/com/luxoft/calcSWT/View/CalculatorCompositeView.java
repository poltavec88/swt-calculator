package main.java.com.luxoft.calcSWT.View;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;

import main.java.com.luxoft.calcSWT.controller.DateModifyListener;
import main.java.com.luxoft.calcSWT.controller.InputsVerfiticalionListener;
import main.java.com.luxoft.calcSWT.controller.SelectionButtonListener;
import main.java.com.luxoft.calcSWT.model.Operand;
import main.java.com.luxoft.calcSWT.service.Calculateble;

public class CalculatorCompositeView extends Composite implements Calculateble{
	
	private Combo operand;
	private Text input1;	
	private Text input2;
	private Text result;
	private Button calcButton;
	private Button flyButton;
	private Label lable;
	private Label delay;
	
	private final int MARGIN_AREA_OF_COMPOSITE_ON_THE_RIGHT_SIDE = 5;
	private final int MARGIN_AREA_OF_COMPOSITE_ON_THE_LEFT_SIDE = 5;
	private final int MARGIN_AREA_OF_COMPOSITE_ON_THE_BOTTOM = 5;
	private final int MARGIN_AREA_OF_COMPOSITE_ON_THE_TOP = 5;
	private final int SPACE_FROM_COMPOSITE_BORDER_TO_FILLING_BY_WIDTH = 5;
	private final int SPACE_FROM_COMPOSITE_BORDER_TO_FILLING_BY_HEIGHT = 10;
	private final int NUMBER_OF_COMPOSITE_COLUMNS = 3;
	private final int LAYOUT_SPACING = 5;
	private final int COMPUTE_SIZE_WIDTH = 250;
	private final int COMPUTE_SIZE_HEIGHT = 350;

	private final int BOX_SIZE = 1;
	private final int DOUBLE_BOX_SIZE = 2;
	private final int TRIPLE_BOX_SIZE = 3;	
	
	public CalculatorCompositeView(Composite compositOwner, int compositeStyle) {
		super(compositOwner, compositeStyle);
		
		computeSize(COMPUTE_SIZE_WIDTH, COMPUTE_SIZE_HEIGHT);
		GridLayout layout = new GridLayout();
		layout.marginRight = MARGIN_AREA_OF_COMPOSITE_ON_THE_RIGHT_SIDE;
		layout.marginLeft = MARGIN_AREA_OF_COMPOSITE_ON_THE_LEFT_SIDE;
		layout.marginBottom = MARGIN_AREA_OF_COMPOSITE_ON_THE_BOTTOM;
		layout.marginTop = MARGIN_AREA_OF_COMPOSITE_ON_THE_TOP;
		layout.marginWidth = SPACE_FROM_COMPOSITE_BORDER_TO_FILLING_BY_WIDTH;
		layout.marginHeight = SPACE_FROM_COMPOSITE_BORDER_TO_FILLING_BY_HEIGHT;
		layout.numColumns = NUMBER_OF_COMPOSITE_COLUMNS;
		layout.verticalSpacing = LAYOUT_SPACING;
		layout.makeColumnsEqualWidth = true;
		layout.horizontalSpacing = LAYOUT_SPACING;
		setLayout(layout);
		
		// First number
		input1 = new Text (this, SWT.BORDER);
		GridData gridData = new GridData(SWT.FILL, SWT.FILL, true, false, BOX_SIZE, BOX_SIZE);
		input1.setLayoutData(gridData);
		input1.addListener(SWT.Verify, new InputsVerfiticalionListener(input1));
		input1.setToolTipText("Number only allowed");
		input1.addModifyListener(new DateModifyListener(this));
	
		// Operand
		operand = new Combo (this, SWT.READ_ONLY | SWT.BORDER);
		operand.setItems(new String[] { Operand.PLUS.getValue(), Operand.MINUS.getValue(), 
				Operand.MULTIPLY.getValue(), Operand.DIVIDE.getValue()});
		operand.setText(Operand.PLUS.getValue());
		operand.setLayoutData(gridData);	
		operand.addModifyListener(new DateModifyListener(this));
	
		// Second number
		input2 = new Text (this, SWT.BORDER);
		input2.setLayoutData(gridData);
		input2.addListener(SWT.Verify, new InputsVerfiticalionListener(input2));
		input2.setToolTipText("Number only allowed");
		input2.addModifyListener(new DateModifyListener(this));
	
		// Delay
		delay = new Label(this, SWT.NULL);
		delay.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, TRIPLE_BOX_SIZE, BOX_SIZE));

		// "Calculate on the fly" check button
		flyButton = new Button(this, SWT.CHECK);
		flyButton.setText("Calculate on the fly");
		flyButton.setSelection(false);
		flyButton.setLayoutData(new GridData(SWT.FILL, SWT.FILL, false, false, DOUBLE_BOX_SIZE, BOX_SIZE));
		flyButton.addSelectionListener(new SelectionListener() {
			@Override
			public void widgetSelected(SelectionEvent arg0) {
				if (flyButton.getSelection() == true) calcButton.setEnabled(false); else calcButton.setEnabled(true);
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent arg0) {
			}
		});

		// "Calculate" push button
		calcButton = new Button(this, SWT.PUSH);
		calcButton.setText("Calculate");
		calcButton.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, BOX_SIZE, BOX_SIZE)); 
		calcButton.pack();
		calcButton.addSelectionListener(new SelectionButtonListener(this));
		calcButton.setToolTipText("Turns to disabled if calculated on the fly");
    
		// "Result" label
		lable = new Label(this, SWT.NULL);
		lable.setText("Result:");
		lable.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, BOX_SIZE, BOX_SIZE));
    
		// Result text
		result = new Text(this, SWT.READ_ONLY | SWT.BORDER | SWT.BOTTOM);
		result.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, DOUBLE_BOX_SIZE, BOX_SIZE));
		
	}

	@Override
	public Operand getOperand() {
		return Operand.getFromString(operand.getText());
	}

	@Override
	public double getInput1() {
		double answer = Double.parseDouble(input1.getText());
		return answer;
	}

	@Override
	public double getInput2() {
		return Double.parseDouble(input2.getText());
	}

	@Override
	public void setResult(Double result) {
		this.result.setText(String.valueOf(result));
	}

	@Override
	public boolean isOnFly() {
		return flyButton.getSelection();
	}
}
