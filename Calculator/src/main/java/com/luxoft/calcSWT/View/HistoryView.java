package main.java.com.luxoft.calcSWT.View;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.SashForm;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.List;

import main.java.com.luxoft.calcSWT.model.CalculatorModel;
import main.java.com.luxoft.calcSWT.service.HistoryService;
import main.java.com.luxoft.calcSWT.service.Updateble;

public class HistoryView extends SashForm implements Updateble {
	
	private List history;

	public HistoryView(Composite sashFormOwner, int sashFormStyle) {
		super(sashFormOwner, sashFormStyle);

    	history = new List(this, SWT.V_SCROLL | SWT.CHANGED);
    	
	}

	@Override
	public void update(CalculatorModel calculatorModel) {
		history.add(HistoryService.getHistoryService().toStringHistory(HistoryService.getHistoryService().getHisSize() - 1));	
	}
}
