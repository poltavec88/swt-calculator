package main.java.com.luxoft.calcSWT.View;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.TabFolder;
import org.eclipse.swt.widgets.TabItem;

import main.java.com.luxoft.calcSWT.model.CalculatorModel;
import main.java.com.luxoft.calcSWT.service.HistoryService;
import main.java.com.luxoft.calcSWT.service.Updateble;

public class View implements Updateble {
	
	private Label label;
	private TabFolder folder;
	private HistoryView hisComposite;
	private Composite calcComposite;
	
	private final int SHELL_SIZE_WIDTH = 300;
	private final int SHELL_SIZE_HEIGHT = 420;
	
	private final int ZERO_FORM_ATTACHMENT = 0;
	private final int FULL_FORM_ATTACHMENT = 100;
	private final int INDENT_FORM_ATTACHMENT = 95;
	 
    public void show() {
    	Display display = new Display ();
    	Shell shell = new Shell(display);
    	shell.setText("SWT Calc"); 
    	shell.setSize(SHELL_SIZE_WIDTH, SHELL_SIZE_HEIGHT);
    	FormLayout fLayout = new FormLayout();
    	shell.setLayout(fLayout);
    	
    	folder = new TabFolder(shell, SWT.NONE);
    	FormData folderData = new FormData();
    	folderData.left = new FormAttachment(ZERO_FORM_ATTACHMENT);
    	folderData.right = new FormAttachment(FULL_FORM_ATTACHMENT);
    	folderData.bottom = new FormAttachment(INDENT_FORM_ATTACHMENT);
    	folderData.top = new FormAttachment(ZERO_FORM_ATTACHMENT);
    	folder.setLayoutData(folderData);
    	
    	// status bar
    	label = new Label(shell, SWT.BORDER);
    	label.setText("Empty history");
    	FormData labelData = new FormData();
        labelData.left = new FormAttachment(ZERO_FORM_ATTACHMENT);
        labelData.right = new FormAttachment(FULL_FORM_ATTACHMENT);
        labelData.bottom = new FormAttachment(FULL_FORM_ATTACHMENT);
        label.setLayoutData(labelData);
        hisComposite = new HistoryView(folder, SWT.DEFAULT);
        
    	//Calculator
        TabItem tab1 = new TabItem(folder, SWT.NONE);
    	tab1.setText("Calculator");    	    	
    	calcComposite = new CalculatorCompositeView(folder, SWT.BORDER);
        tab1.setControl(calcComposite);	
        
        //History
        TabItem tab2 = new TabItem(folder, SWT.NONE);
    	tab2.setText("History");
    	tab2.setToolTipText("Contains the list of previous calculations");
    	tab2.setControl(hisComposite);
    	
    	HistoryService.getHistoryService().addSubscriber(hisComposite);
    	HistoryService.getHistoryService().addSubscriber(this);
           
    	shell.open();
    	while (!shell.isDisposed()) {
    		if (!display.readAndDispatch())
    			display.sleep();
    	}    		
    display.dispose();
    
    
    }

	@Override
	public void update(CalculatorModel calculatorModel) {
		label.setText("Last operation: " + calculatorModel.toString());
	}
	
}