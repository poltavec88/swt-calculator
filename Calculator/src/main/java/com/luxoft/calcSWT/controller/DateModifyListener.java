package main.java.com.luxoft.calcSWT.controller;

import java.math.BigDecimal;

import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;

import main.java.com.luxoft.calcSWT.service.Calculateble;
import main.java.com.luxoft.calcSWT.model.CalculatorModel;
import main.java.com.luxoft.calcSWT.model.Operand;
import main.java.com.luxoft.calcSWT.service.CalculatorService;

public class DateModifyListener implements ModifyListener  {

	private Calculateble calculateble;
	
	public DateModifyListener(Calculateble calculateble) {
		this.calculateble = calculateble;
	}

	@Override
	public void modifyText(ModifyEvent e) {
		if (calculateble.isOnFly()) {			
			try {
			
				BigDecimal i1 = BigDecimal.valueOf(calculateble.getInput1());
				Operand op = calculateble.getOperand();
				BigDecimal i2 = BigDecimal.valueOf(calculateble.getInput2());
				CalculatorModel answer = CalculatorService.operationPerform(i1, op, i2);
				calculateble.setResult(Double.valueOf(answer.getResult().toString()));
				
			} catch (NumberFormatException ex) {
			}
		} 
	}
}