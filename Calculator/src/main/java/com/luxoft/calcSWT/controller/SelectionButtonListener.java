package main.java.com.luxoft.calcSWT.controller;

import java.math.BigDecimal;

import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;

import main.java.com.luxoft.calcSWT.model.CalculatorModel;
import main.java.com.luxoft.calcSWT.model.Operand;
import main.java.com.luxoft.calcSWT.service.Calculateble;
import main.java.com.luxoft.calcSWT.service.CalculatorService;
import main.java.com.luxoft.calcSWT.service.HistoryService;

public class SelectionButtonListener implements SelectionListener{
	
	private Calculateble calculateble;
	
	public SelectionButtonListener(Calculateble calculateble) {
		this.calculateble = calculateble;
	}

	@Override
	public void widgetSelected(SelectionEvent arg0) {
		
		try {
			
			BigDecimal i1 = BigDecimal.valueOf(calculateble.getInput1());
			Operand op = calculateble.getOperand();
			BigDecimal i2 = BigDecimal.valueOf(calculateble.getInput2());
			CalculatorModel answer = CalculatorService.operationPerform(i1, op, i2);
			calculateble.setResult(Double.valueOf(answer.getResult().toString()));
			HistoryService.getHistoryService().addHistoryItem(answer);
		
		} 
		
		catch (NumberFormatException e) {
			
		}
	}

	@Override
	public void widgetDefaultSelected(SelectionEvent arg0) {
	}
}
