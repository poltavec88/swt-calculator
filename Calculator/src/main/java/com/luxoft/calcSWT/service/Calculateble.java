package main.java.com.luxoft.calcSWT.service;

import main.java.com.luxoft.calcSWT.model.Operand;

public interface Calculateble {
	
	public Operand getOperand();
	
	public double getInput1();
	
	public double getInput2();
	
	public void setResult(Double double1);
	
	public boolean isOnFly();
	
}
