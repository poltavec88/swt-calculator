package main.java.com.luxoft.calcSWT.service;

import java.util.ArrayList;
import java.util.List;

import main.java.com.luxoft.calcSWT.model.CalculatorModel;
import main.java.com.luxoft.calcSWT.model.HistoryModel;

public class HistoryService {
		
	private static HistoryService historyService;
	private final static HistoryModel historyModel = new HistoryModel();
	private final static List<Updateble> subscribers = new ArrayList<>();

	private HistoryService() {
	}
	
	public static HistoryService getHistoryService() {
		if (historyService == null) {
			return new HistoryService();
		}
		return historyService;
	}
	
	public void addHistoryItem(CalculatorModel calculatorModel) {
		historyModel.getInput1().add(calculatorModel.getFirstNumber());
		historyModel.getInput2().add(calculatorModel.getSecondNumber());
		historyModel.getOperand().add(calculatorModel.getOperand());
		historyModel.getResult().add(calculatorModel.getResult());
		notifysubscribers(calculatorModel);
	}
	
	public String toStringHistory(int i) {
		return historyModel.toStringHis(i);
	}
	
	public int getHisSize() {
		return historyModel.getInput1().size();
	}
	
	public boolean addSubscriber(Updateble updateble) {
		return subscribers.add(updateble);
	}
	
	public void notifysubscribers(CalculatorModel calculatorModel) {
		for (Updateble updateble: subscribers) {
			updateble.update(calculatorModel);
		}
	}
}
