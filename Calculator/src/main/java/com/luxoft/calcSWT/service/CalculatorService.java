package main.java.com.luxoft.calcSWT.service;

import java.math.BigDecimal;
import java.math.RoundingMode;

import main.java.com.luxoft.calcSWT.model.CalculatorModel;
import main.java.com.luxoft.calcSWT.model.Operand;

public class CalculatorService {
	
	public static CalculatorModel operationPerform(BigDecimal i1, Operand op, BigDecimal i2) {
		
		CalculatorModel result = new CalculatorModel();
			
		result.setFirstNumber(i1);
		result.setSecondNumber(i2);
		switch (op) {
           case PLUS: 
        	   
        	   result.setResult(i1.add(i2)); 
        	   result.setOperand(Operand.PLUS); 
        	   break;
        	   
           case MINUS: 
        	   
        	   result.setResult(i1.subtract(i2)); 
        	   result.setOperand(Operand.MINUS); 
        	   break;
        	   
           case MULTIPLY: 
        	   
        	   result.setResult(i1.multiply(i2)); 
        	   result.setOperand(Operand.MULTIPLY); 
        	   break;
        	   
           case DIVIDE: 
        	   
        	   result.setResult((i1.divide(i2, 15, RoundingMode.HALF_UP))); 
        	   result.setOperand(Operand.DIVIDE); 
        	   break;
        	   
           default: 
        	   
        	   throw new RuntimeException("impossible operator");
        }
		
		
		return result;
	};

}
