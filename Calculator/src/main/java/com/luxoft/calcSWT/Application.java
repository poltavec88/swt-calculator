package main.java.com.luxoft.calcSWT;

import main.java.com.luxoft.calcSWT.View.View;

public class Application {
	public static void main (String[] args) {
        View view = new View();
        view.show();
	}
}